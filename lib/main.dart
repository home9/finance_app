//* Icons made by Kiranshastry from https://www.flaticon.com/

import 'package:finance_app/src/providers/google_signin_provider.dart';
import 'package:finance_app/src/providers/login_provider.dart';
import 'package:finance_app/src/route.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';

// void main() => runApp(_MyApp());

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  // ignore: prefer_final_locals
  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );
  runApp(_MyApp());
}

class _MyApp extends StatelessWidget {
  // ignore: unused_field
  // final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => GoogleSignInProvider()),
        ChangeNotifierProvider(create: (_) => LoginProvider())
      ],
      child: MaterialApp(
        theme: ThemeData(
          brightness: Brightness.light,
          primarySwatch: Colors.teal,
        ),
        darkTheme: ThemeData(
          primarySwatch: Colors.amber,
          brightness: Brightness.dark,
        ),
        title: 'F-App JS',
        initialRoute: routeByPlatform('/intro'),
        routes: appRoutes,
      ),
    );
  }
}
