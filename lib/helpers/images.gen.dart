class IMG {
  IMG._();
  static final icons = _Icons();
}

class _Icons {
  String get monedaPNG => "assets/images/icons/moneda.png";
  String get settingsPNG => "assets/images/icons/settings.png";
  String get undrawMobilePayReSjb8SVG => "assets/images/icons/undraw_mobile_pay_re_sjb8.svg";
  String get undrawCreditCardSVG => "assets/images/icons/undraw_credit_card.svg";
  String get undrawOnlineTransactions_02kaSVG => "assets/images/icons/undraw_online_transactions_02ka.svg";
  String get undrawPayOnlineB1hkSVG => "assets/images/icons/undraw_pay_online_b1hk.svg";
  String get undrawPersonalFinanceSVG => "assets/images/icons/undraw_personal_finance.svg";
  String get undrawSavingsSVG => "assets/images/icons/undraw_savings.svg";
  String get undrawSecuritySVG => "assets/images/icons/undraw_security.svg";
  String get walletUndrawSVG => "assets/images/icons/wallet_undraw.svg";
}
