import 'dart:io' show Platform;

import 'package:finance_app/src/screens/common/intro_screen.dart';
import 'package:finance_app/src/screens/common/login_screen.dart';
import 'package:flutter/material.dart';

// only Android iOS and web
String routeByPlatform(String route) {
  if (Platform.isAndroid) {
    return '${route}_android';
  } else {
    return route;
  }
}

void goTo(BuildContext context, String route) {
  Navigator.pushNamed(context, routeByPlatform(route));
}

Map<String, Widget Function(BuildContext)> appRoutes = {
  '/intro': (BuildContext _) => const IntroScreen(),
  '/intro_android': (BuildContext _) => const IntroScreen(),
  '/login': (BuildContext _) => const LoginScreen(),
  '/login_android': (BuildContext _) => const LoginScreen()
};
