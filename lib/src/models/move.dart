import 'package:finance_app/src/utils/date.dart';
import 'package:flutter/material.dart';

class Movement {
  late IconData reasonIcon;
  late String reason;
  late DateTime date;
  late double amount;
  late bool isIncome;

  @override
  String toString() {
    return 'S./$amount - $reason ${getDate(date)}';
  }
}
