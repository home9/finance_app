// ignore_for_file: avoid_unused_constructor_parameters, sort_constructors_first

import 'dart:convert';

class User {
  User({
    String username = '',
    String avatar = '',
  });

  String username = '';
  String avatar = '';

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'username': username,
      'avatar': avatar,
    };
  }

  factory User.fromMap(Map<String, String> map) {
    return User(
      username: map['username']!,
      avatar: map['avatar']!,
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) =>
      User.fromMap(json.decode(source) as Map<String, String>);
}
