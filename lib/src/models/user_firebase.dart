// import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class UserFirebase with ChangeNotifier {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  bool _isSignIn = false;

  bool get isSignIn => _isSignIn;

  set isSignIn(bool isSignIn) {
    _isSignIn = isSignIn;
    notifyListeners();
  }

  Future<void> googleSignIn() async {
    try {
      isSignIn = true;
      final user = await _googleSignIn.signIn();
      // return result;
      if (user == null) {
        isSignIn = false;
        return;
      }

      final googleAuth = await user.authentication;
      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      await FirebaseAuth.instance.signInWithCredential(credential);
      isSignIn = false;
    } catch (e) {
      // notifyListeners();
      return;
    }
  }

  Future<void> googleLogout() async {
    try {
      await _googleSignIn.disconnect();
      await FirebaseAuth.instance.signOut();
    } catch (e) {
      // notifyListeners();
      return;
    }
  }
}
