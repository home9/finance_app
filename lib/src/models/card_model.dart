import 'package:finance_app/src/constants/bank.dart';

class CardBankModel {
  late String title;
  late String nro;
  late String nroCCI;
  late String amount;
  late Banks bank;
  late FinancialService financialService;
}
