import 'package:cached_network_image/cached_network_image.dart';
import 'package:finance_app/src/utils/responsive.dart';
import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HeaderCustom extends StatelessWidget {
  const HeaderCustom({required this.avatar, required this.username, Key? key})
      : super(key: key);
  final String avatar;
  final String username;

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SafeArea(
          child: Container(
            padding:
                const EdgeInsets.symmetric(vertical: 30.0, horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: responsive.wp(60.0),
                  child: Text(
                    'Hola $username',
                    overflow: TextOverflow.ellipsis,
                    textScaleFactor: 1.0,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: responsive.dp(2.5),
                      // fontSize: height * 0.03,
                    ),
                  ),
                ),
                Text(
                  'Bienvenido a tu billetera',
                  textScaleFactor: 1.0,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: responsive.dp(1.5),
                  ),
                )
              ],
            ),
          ),
        ),
        SafeArea(
          child: Container(
            padding: const EdgeInsets.symmetric(
              vertical: 15.0,
            ),
            child: GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/settings');
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: Hero(
                  tag: 'userImg',
                  child: CircleAvatar(
                    maxRadius: responsive.dp(3.5),
                    backgroundColor: Colors.white,
                    backgroundImage: CachedNetworkImageProvider(avatar),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
