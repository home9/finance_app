import 'package:finance_app/src/utils/responsive.dart';
import 'package:flutter/material.dart';
import 'package:finance_app/src/providers/card_provider.dart';
import 'package:finance_app/src/widgets/card.dart';

class CardList extends StatelessWidget {
  final PageController _pageController = PageController();
  final Function callback;

  CardList({required this.callback});

  @override
  // ignore: override_on_non_overriding_member
  void dispose() {
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _cardRow(context),
    );
  }

  List<Widget> _cards(double height, double width, BuildContext context) {
    CardsProvider cardDB = CardsProvider();

    return cardDB
        .getUserCards()
        .map(
          (e) => GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/cardmove', arguments: {
                'height': height,
                'width': width,
                'cardData': e,
              });
            },
            child: CardBank(height, width, e),
          ),
        )
        .toList();
  }

  Row _cardRow(BuildContext context) {
    final responsive = Responsive.of(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: responsive.hp(25.0),
          width: responsive.wp(75.0),
          padding: EdgeInsets.all(10.0),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: PageView(
              physics: BouncingScrollPhysics(),
              controller: _pageController,
              scrollDirection: Axis.vertical,
              children:
                  _cards(responsive.hp(25.0), responsive.wp(75.0), context),
              onPageChanged: (value) async {
                //await Future.delayed(Duration(milliseconds: 1200));
                if (value == _pageController.page) {
                  callback(value);
                }
              },
            ),
          ),
        ),
        SizedBox(
          width: 20.0,
        ),
        CircleAvatar(
          backgroundColor: Colors.grey[200],
          child: IconButton(
            icon: Icon(Icons.add),
            color: Colors.black,
            onPressed: () {
              print('plus');
            },
          ),
        )
      ],
    );
  }
}
