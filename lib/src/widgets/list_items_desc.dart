import 'package:finance_app/src/models/move.dart';
import 'package:finance_app/src/utils/date.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ListItems extends StatelessWidget {
  final Orientation orientation;
  final int cardIndex;
  final bool isIn;
  final Function callback;
  final List<Movement> getMoves;

  ListItems(this.orientation, this.getMoves, this.cardIndex, this.callback,
      {this.isIn = true});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RefreshIndicator(
        child: _list(orientation, context),
        onRefresh: () async {
          // await Future.delayed(
          //   Duration(
          //     seconds: 1,
          //   ),
          // );
          callback(cardIndex);
        },
      ),
    );
  }

  Container _list(Orientation orientation, BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: MediaQuery.removePadding(
        removeTop: true,
        context: context,
        child: ListView.builder(
          physics: BouncingScrollPhysics(),
          itemCount: getMoves.length,
          itemBuilder: (BuildContext context, int index) {
            return _item(movement: getMoves[index], status: isIn);
          },
        ),
      ),
    );
  }

  ListTile _item({
    required Movement movement,
    bool status = true,
  }) {
    return ListTile(
      leading: FaIcon(movement.reasonIcon),
      title: Text(
        movement.reason,
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        getDate(movement.date),
        textScaleFactor: 1.0,
      ),
      trailing: Text(
        'S./ ${movement.amount}',
        textScaleFactor: 1.0,
        style: TextStyle(
          color: status ? Colors.green[900] : Colors.red[500],
          fontSize: 20.0,
        ),
      ),
      onTap: () {},
    );
  }
}
