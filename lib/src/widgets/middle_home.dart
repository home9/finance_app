import 'package:finance_app/src/utils/date.dart';
import 'package:finance_app/src/utils/responsive.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MiddleHomeWidget extends StatelessWidget {
  const MiddleHomeWidget({
    required this.dateMY,
    required this.sum,
    required this.cardIndex,
  }) : super();

  final DateTime dateMY;
  final double sum;
  final int cardIndex;

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'GASTÉ ',
                  textScaleFactor: 1.0,
                  style: TextStyle(
                    fontSize: responsive.dp(1.5),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 0.0,
                  ),
                  child: Text(
                    getDateConsult(dateMY),
                    textScaleFactor: 0.8,
                    style: TextStyle(
                      // color: Colors.grey[600],
                      fontSize: 12.0,
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 15.0,
              ),
              child: Text(
                'S./${(sum).toStringAsFixed(2)}',
                textScaleFactor: 1.0,
                style: TextStyle(
                  color: Colors.red[500],
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
        GestureDetector(
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    right: 10.0,
                  ),
                  child: Text(
                    'Ver todo',
                    textScaleFactor: 1.0,
                  ),
                ),
                FaIcon(
                  FontAwesomeIcons.chevronRight,
                  size: 15.0,
                  // color: Colors.black,
                ),
              ],
            ),
            onTap: () async {
              Navigator.pushNamed(context, '/expenses', arguments: {
                'cardIndex': cardIndex,
              });
            })
      ],
    );
  }
}
