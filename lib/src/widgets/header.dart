import 'package:flutter/material.dart';

class HeaderDiagonal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: CustomPaint(
        painter: _HeaderDiagonalPainter(),
      ),
    );
  }
}

class HeaderCurvo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: CustomPaint(
        painter: _HeaderCurvoPainter(),
      ),
    );
  }
}

class _HeaderCurvoPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint()
      ..color = Colors.black
      ..style = PaintingStyle.fill // .fill .stroke
      ..strokeWidth = 2;

    final path = Path();

    path.lineTo(0, size.height * 0.35);
    path.quadraticBezierTo(size.width * 0.01, size.height * 0.48,
        size.width * 0.2, size.height * 0.5);
    path.lineTo(size.width * 0.8, size.height * 0.5);
    path.quadraticBezierTo(
        size.width * 0.99, size.height * 0.48, size.width, size.height * 0.65);
    path.lineTo(size.width, 0);
    canvas.drawPath(path, lapiz);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class _HeaderDiagonalPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint()
      ..color = Colors.black
      ..style = PaintingStyle.fill // .fill .stroke
      ..strokeWidth = 2;

    final path = Path();

    // Dibujar con el path y el lapiz
    path.lineTo(0, size.height);
    path.lineTo(size.width * 0.45, size.height);
    // path.lineTo(size.width * 0.8, size.height * 0.8);
    path.lineTo(size.width * 0.65, 0);

    canvas.drawPath(path, lapiz);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
