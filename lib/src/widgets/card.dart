import 'package:finance_app/src/utils/responsive.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:finance_app/src/constants/bank.dart';

import 'package:finance_app/src/models/card_model.dart';

import 'package:finance_app/src/widgets/cardBank/BBVA.dart';
import 'package:finance_app/src/widgets/cardBank/BCP.dart';
import 'package:finance_app/src/widgets/cardBank/InterBank.dart';

import 'package:finance_app/src/extensions/financial_service_ext.dart';
import 'package:finance_app/src/utils/encrypt.dart';

class CardBank extends StatelessWidget {
  final double height;
  final double width;
  final CardBankModel cardData;
  CardBank(this.height, this.width, this.cardData);
  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: ConstrainedBox(
        constraints: BoxConstraints(maxHeight: height, maxWidth: width),
        child: Container(
          height: height,
          width: width,
          child: Stack(
            children: [
              if (cardData.bank == Banks.BCP) CardCurvoBCPCredimas(),
              if (cardData.bank == Banks.InterBank) CardCurvoInterBank(),
              if (cardData.bank == Banks.BBVA) CardCurvoBBVACompras(),
              Container(
                padding: EdgeInsets.all(responsive.width <= 320 ? 10.0 : 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      cardData.title,
                      textScaleFactor: 1.0,
                      style: TextStyle(color: Colors.white),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              encrypt(cardData.nro),
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: responsive.dp(1.5)),
                            ),
                          ),
                          Text(
                            'CCI: ${encryptCCI(cardData.nroCCI)}',
                            textScaleFactor: 1.0,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: responsive.dp(1.5)),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 5.0),
                          child: Text(
                            cardData.amount,
                            textScaleFactor: 1.0,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: responsive.dp(1.8)),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 1.0),
                          child: FaIcon(
                            cardData.financialService.icon,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
