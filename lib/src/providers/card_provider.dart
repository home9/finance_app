import 'package:finance_app/src/constants/bank.dart';
import 'package:finance_app/src/models/card_model.dart';

class CardsProvider {
  int cardIndex = 0;
  final List<CardBankModel> _list = [];

  // int get cardIndex => _cardIndex;

  // set cardIndex(int cardIndex) {
  //   _cardIndex = cardIndex;
  // }

  List<CardBankModel> getUserCards() {
    //ignore: todo
    //TODO load database cards

    _list.add(CardBankModel()
      ..title = 'BCP - Cuenta de ahorros en soles'
      ..nro = '570-12345679-0-49'
      ..nroCCI = '003-12345678912354687'
      ..amount = 'S./ 1,001.92'
      ..bank = Banks.BCP
      ..financialService = FinancialService.visa);

    // _list.add(CardBankModel()
    //   ..title = 'BBVA - Cuenta de ahorros en soles'
    //   ..nro = '570-12345678-0-49'
    //   ..nroCCI = '003-12345678912354686'
    //   ..amount = 'S./ 115.88'
    //   ..bank = Banks.BBVA
    //   ..financialService = FinancialService.visa);

    // _list.add(CardBankModel()
    //   ..title = 'Interbank - Cuenta de ahorros en soles'
    //   ..nro = '570-12345677-0-49'
    //   ..nroCCI = '003-12345678912354685'
    //   ..amount = 'S./ 15.88'
    //   ..bank = Banks.InterBank
    //   ..financialService = FinancialService.visa);

    return _list;
  }
}
