import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class GoogleSignInProvider extends ChangeNotifier {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  bool _isSignIn = false;
  bool _isAuth = false;
  bool get isSignIn => _isSignIn;
  bool get isAuth => _isAuth;

  set isSignIn(bool isSignIn) {
    _isSignIn = isSignIn;
    notifyListeners();
  }

  set isAuth(bool isAuth) {
    _isAuth = isAuth;
    notifyListeners();
  }

  Future<void> googleSignIn() async {
    try {
      isSignIn = true;
      final user = await _googleSignIn.signIn();
      // return result;
      if (user == null) {
        isSignIn = false;
        return;
      }

      final googleAuth = await user.authentication;
      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      await FirebaseAuth.instance.signInWithCredential(credential);
      isAuth = true;
      isSignIn = false;
    } catch (e) {
      isSignIn = false;
      isAuth = false;
      // notifyListeners();
      return;
    }
  }

  Future<void> googleLogout() async {
    try {
      await _googleSignIn.disconnect();
      await FirebaseAuth.instance.signOut();
    } catch (e) {
      // notifyListeners();
      return;
    } finally {
      isAuth = false;
    }
  }
}
