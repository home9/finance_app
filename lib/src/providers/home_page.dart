import 'package:finance_app/src/models/move.dart';
import 'package:finance_app/src/providers/card_provider.dart';
import 'package:finance_app/src/providers/move_provider.dart';
import 'package:finance_app/src/utils/bank.dart';
import 'package:finance_app/src/utils/share.dart';
import 'package:flutter/material.dart';

class HomePageProvider extends ChangeNotifier {
  int _pageIndex = 0;
  final int _cardIndex = 0;
  double _sum = 0;
  DateTime _dateMY = DateTime.now();

  int get pageIndex => _pageIndex;
  int get cardIndex => _cardIndex;
  double get sum => _sum;
  DateTime get dateMY => _dateMY;

  void onPageChange(int page) {
    _pageIndex = page;
    notifyListeners();
  }

  Future<void> shareSocialCard(BuildContext context) async {
    final ShareModal shareModal = ShareModal.of(context);
    final CardsProvider cardsProvider = CardsProvider();
    final card = cardsProvider.getUserCards()[_cardIndex];
    shareModal
      ..text =
          // ignore: lines_longer_than_80_chars
          'Mi número de cuenta en ${bankName(card)} \nes ${card.nro}\n y CCI es ${card.nroCCI}'
      ..subject = card.title
      ..socialShare(context);
  }

  Future<void> update() async {
    final MovementProvider moveProvider = MovementProvider();

    final List<Movement> getMovesAsync =
        await moveProvider.getMoves(card: _cardIndex);

    _dateMY = DateTime.now();
    _sum = 0.0;
    for (final element in getMovesAsync) {
      _sum += element.amount;
    }
    notifyListeners();
  }
}
