import 'package:flutter/material.dart';

class LoginProvider extends ChangeNotifier {
  bool isLocked = false;
  bool isRegister = false;

  void setIsLocked(bool isLocked) {
    this.isLocked = isLocked;
    notifyListeners();
  }

  void setIsRegister(bool isRegister) {
    this.isRegister = isRegister;
    notifyListeners();
  }
}
