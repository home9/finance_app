import 'package:finance_app/src/models/move.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MovementProvider {
  Future<List<Movement>> getMoves({int card = 0}) async {
    List<Movement> list = [];

    if (card == 0) {
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.shoppingBag
        ..reason = 'Compras'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 50.00);
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.graduationCap
        ..reason = 'Udemy'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 75.30);
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.shoppingBasket
        ..reason = 'Alimentos'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 20.00);
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.church
        ..reason = 'Diezmo'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 15.00);

      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.university
        ..reason = 'Sunat'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 33.00);
    }
    if (card == 1) {
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.shoppingBag
        ..reason = 'Compras'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 20.00);
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.graduationCap
        ..reason = 'Udemy'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 75.30);
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.tint
        ..reason = 'Sedalib'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 22.30);

      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.bolt
        ..reason = 'Hidrandina'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 22.85);
    }
    if (card == 2) {
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.church
        ..reason = 'Diezmo'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 15.00);
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.tshirt
        ..reason = 'Ropa'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 25.00);
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.graduationCap
        ..reason = 'Udemy'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 22.30);
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.internetExplorer
        ..reason = 'Internet'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 22.85);
      list.add(Movement()
        ..reasonIcon = FontAwesomeIcons.gasPump
        ..reason = 'Quavi'
        ..date = DateTime(2020, 8, 20, 14, 0)
        ..amount = 22.83);
    }
    return list;
  }
}
