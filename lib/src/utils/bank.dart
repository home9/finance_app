import 'package:finance_app/src/models/card_model.dart';

String bankName(CardBankModel card) {
  return card.bank.toString().substring(card.bank.toString().indexOf('.') + 1);
}
