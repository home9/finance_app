import 'dart:io';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class DevicePlatform {
  bool isWindows = false;
  bool isAndroid = false;
  bool isIOS = false;
  bool isLinux = false;
  bool isMacOS = false;
  bool isWeb = false;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  void getDevicePlatform() {
    isWindows = Platform.isWindows;
    isAndroid = Platform.isAndroid;
    isIOS = Platform.isIOS;
    isLinux = Platform.isLinux;
    isMacOS = Platform.isMacOS;
    isWeb = kIsWeb;
  }

  Future<String> getDeviceDetail() async {
    if (isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      // ignore: todo
      // print(androidInfo.systemFeatures); //TODO check avilable features
      return ' ${androidInfo.manufacturer} ${androidInfo.model}';
    }

    if (isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      // ignore: unnecessary_string_interpolations
      return '${iosInfo.utsname.machine}';
    }

    if (isWeb) {
      WebBrowserInfo webBrowserInfo = await deviceInfo.webBrowserInfo;
      // ignore: unnecessary_string_interpolations
      return '${webBrowserInfo.userAgent}';
    }

    return '';
  }
}
