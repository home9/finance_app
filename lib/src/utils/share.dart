import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';

class ShareModal {
  // ignore: avoid_unused_constructor_parameters
  ShareModal(BuildContext context);
  String _text = '';
  String _subject = '';

  set text(String text) {
    _text = text;
  }

  set subject(String subject) {
    _subject = subject;
  }

  static ShareModal of(BuildContext context) => ShareModal(context);

  void socialShare(BuildContext context) {
    // final RenderBox box = context.findRenderObject();
    Share.share(
      _text,
      subject: _subject,
      // sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size,
    );
  }
}
