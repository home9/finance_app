import 'package:simple_moment/simple_moment.dart';

String getDate(DateTime date) {
  // Moment moment = Moment.fromDate(date).locale(LocaleEs());
  Moment moment =
      // Moment.fromDate(DateTime.utc(2020, 1, 20, 16, 30, 05)).locale(LocaleEs());
      Moment.fromDate(date).locale(LocaleEs());
  // Moment moment = Moment.fromDate(date).locale(LocaleEs(), useInFormat: true);

  return moment.format("dd MMM yyyy, HH:mm");
}

String getDateConsult(DateTime date) {
  // Moment moment = Moment.fromDate(date).locale(LocaleEs());
  Moment moment = Moment.fromDate(date).locale(LocaleEs());
  // Moment moment = Moment.fromDate(date).locale(LocaleEs(), useInFormat: true);

  return moment.format("MM/yyyy");
}
