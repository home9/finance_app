// import 'package:finance_app/src/providers/google_signin_provider.dart';
import 'package:finance_app/src/providers/home_page.dart';
import 'package:finance_app/src/utils/responsive.dart';
// import 'package:finance_app/src/utils/share.dart';
import 'package:finance_app/src/widgets/header.dart';
import 'package:finance_app/src/widgets/header_custom.dart';
// import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    // final provider = Provider.of<GoogleSignInProvider>(context, listen: false);

    return WillPopScope(
      onWillPop: () async => false,
      child: MultiProvider(
        providers: [ChangeNotifierProvider(create: (_) => HomePageProvider())],
        child: LayoutBuilder(
          builder: (context, constraints) {
            if (responsive.orientation == Orientation.portrait) {
              return Scaffold(
                body: Stack(
                  children: [
                    HeaderCurvo(),
                    SingleChildScrollView(
                      child: Column(
                        children: const [
                          SizedBox(
                            height: 200.0,
                            child: InitScreen(),
                          ),

                          // SizedBox(
                          //   height: 100.0,
                          //   child: ElevatedButton(
                          //       onPressed: () {
                          //         ShareModal(context)
                          //           ..text = 'Mi ejemplo de share\nNueva línea2'
                          //           ..subject = 'Asunto'
                          //           ..socialShare(context);
                          //       },
                          //       child: const Text('Social')),
                          // ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }
            return Scaffold(
              body: Stack(
                children: [
                  HeaderDiagonal(),
                  ListView(
                      physics: const BouncingScrollPhysics(),
                      children: const [
                        SizedBox(
                          height: 200.0,
                          child: InitScreen(),
                        )
                      ]),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

class InitScreen extends StatelessWidget {
  const InitScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container();
    // final user = FirebaseAuth.instance.currentUser;
    // return Column(
    //   children: [
    //     Expanded(
    //       flex: 4,
    //       child: HeaderCustom(
    //         avatar: user != null
    //             ? user.photoURL!
    //             : 'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    //         username: user?.displayName ?? 'José',
    //       ),
    //     ),
    //   ],
    // );
  }
}
