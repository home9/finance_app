// ignore_for_file: library_private_types_in_public_api, flutter_style_todos

import 'package:cached_network_image/cached_network_image.dart';
import 'package:finance_app/src/providers/google_signin_provider.dart';
import 'package:finance_app/src/utils/responsive.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class PinScreen extends StatefulWidget {
  const PinScreen({Key? key}) : super(key: key);

  @override
  _PinScreenState createState() => _PinScreenState();
}

class _PinScreenState extends State<PinScreen> {
  final formKey = GlobalKey<FormState>();
  final pinController = TextEditingController();
  final confirmPinController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser;
    // final responsive = Responsive.of(context);
    // ignore: todo
    //TODO check register using Future Builder
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Center(
          child: SafeArea(
              child: OrientationBuilder(
            builder: (_, orientation) => orientation == Orientation.portrait
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      LeftSideRegister(
                        user: user,
                        formKey: formKey,
                        confirmPinController: confirmPinController,
                        pinController: pinController,
                      ),
                      RightSide(
                        formKey: formKey,
                        confirmPinController: confirmPinController,
                        pinController: pinController,
                      ),
                    ],
                  )
                : Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      LeftSideRegister(
                        user: user,
                        formKey: formKey,
                        confirmPinController: confirmPinController,
                        pinController: pinController,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(18),
                        child: RightSide(
                          formKey: formKey,
                          confirmPinController: confirmPinController,
                          pinController: pinController,
                        ),
                      ),
                    ],
                  ),
          )),
        ),
      ),
    );
  }
}

class LeftSideRegister extends StatelessWidget {
  const LeftSideRegister({
    Key? key,
    this.user,
    required this.formKey,
    required this.confirmPinController,
    required this.pinController,
  }) : super(key: key);
  final User? user;
  final GlobalKey<FormState> formKey;
  final TextEditingController pinController;
  final TextEditingController confirmPinController;

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Expanded(
      child: SizedBox(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (responsive.orientation != Orientation.landscape)
                SizedBox(
                  height: responsive.hp(20),
                )
              else
                const SizedBox(),
              Hero(
                tag: 'userImg',
                child: CircleAvatar(
                  maxRadius: responsive.dp(5),
                  backgroundImage: CachedNetworkImageProvider(user?.photoURL ??
                      'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'),
                ),
              ),
              SizedBox(
                height: responsive.hp(10),
              ),
              Text(
                user?.displayName ?? 'Usuario',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: responsive.dp(1.8),
                ),
              ),
              SizedBox(
                height: responsive.hp(5),
              ),
              Container(
                constraints: const BoxConstraints(
                  maxWidth: 500,
                ),
                // height: responsive.hp(20),

                width: responsive.wp(50),
                child: PinForm(
                  formKey: formKey,
                  pinController: pinController,
                  responsive: responsive,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PinForm extends StatelessWidget {
  const PinForm({
    Key? key,
    required this.formKey,
    required this.pinController,
    required this.responsive,
  }) : super(key: key);

  final GlobalKey<FormState> formKey;
  final TextEditingController pinController;
  final Responsive responsive;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          TextFormField(
            controller: pinController,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Debe ingresar su PIN';
              }
              if (value.length != 6) {
                return 'El PIN debe tener sólo 6 dígitos';
              }
              return null;
            },
            obscureText: true,
            decoration: InputDecoration(
              errorMaxLines: 2,
              labelText: 'Escribe tu PIN de seguridad',
              labelStyle: TextStyle(
                fontSize: responsive.dp(1.3),
              ),
            ),
            style: TextStyle(
              fontSize: responsive.dp(1.5),
            ),
          ),
          const SizedBox(height: 30),
        ],
      ),
    );
  }
}

class RightSide extends StatelessWidget {
  const RightSide({
    Key? key,
    required this.formKey,
    required this.confirmPinController,
    required this.pinController,
  }) : super(key: key);
  final GlobalKey<FormState> formKey;
  final TextEditingController pinController;
  final TextEditingController confirmPinController;

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<GoogleSignInProvider>(context, listen: true);
    final responsive = Responsive.of(context);
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: responsive.dp(10),
            child: IconButton(
              onPressed: () async {
                if (formKey.currentState!.validate()) {
                  formKey.currentState!.save();
                  await Navigator.pushNamed(context, '/home');
                }
              },
              icon: const Icon(Icons.keyboard_arrow_right_sharp),
            ),
          ),
          SizedBox(height: responsive.hp(1)),
          SizedBox(
            width: responsive.dp(20),
            child: TextButton(
              onPressed: () async {
                await provider.googleLogout();
                await Navigator.pushNamed(context, '/login');
              },
              child: Text(
                'Cambiar a otra cuenta - pin',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: responsive.dp(1.2),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
