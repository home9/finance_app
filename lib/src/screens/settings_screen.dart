// import 'package:finance_app/helpers/images.gen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:finance_app/src/providers/google_signin_provider.dart';
import 'package:finance_app/src/utils/responsive.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingScreen extends StatelessWidget {
  const SettingScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser;
    final provider = Provider.of<GoogleSignInProvider>(context, listen: false);
    final responsive = Responsive.of(context);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 30.0,
              ),
              Hero(
                tag: 'userImg',
                child: CircleAvatar(
                  maxRadius: responsive.dp(5.5),
                  backgroundColor: Colors.white,
                  backgroundImage: CachedNetworkImageProvider(user?.photoURL ??
                      'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'),
                ),
              ),
              Container(
                height: 30.0,
              ),
              ElevatedButton(
                onPressed: () async {
                  await provider.googleLogout();
                  await Navigator.pushNamed(context, '/login');
                },
                child: const Text('Cerrar Sesión'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
