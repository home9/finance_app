import 'package:finance_app/src/preferences/card.dart';
import 'package:finance_app/src/providers/home_page.dart';
import 'package:finance_app/src/utils/responsive.dart';
import 'package:finance_app/src/widgets/header_custom.dart';
import 'package:finance_app/src/widgets/middle_home.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
// import 'package:share/share.dart';
import 'package:share_plus/share_plus.dart';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';

import 'package:finance_app/src/models/move.dart';
import 'package:finance_app/src/providers/card_provider.dart';
import 'package:finance_app/src/utils/bank.dart';
import 'package:finance_app/src/providers/move_provider.dart';
import 'package:finance_app/src/widgets/card_list.dart';
import 'package:finance_app/src/widgets/header.dart';
import 'package:finance_app/src/widgets/list_items_desc.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int cardIndex = 0;
  double sum = 0;
  DateTime dateMY = DateTime.now();
  List<Movement> getMoves = <Movement>[];
  int pageIndex = 0;

  String text = '';
  String subject = '';
  PageController _pageController =
      PageController(initialPage: 0, keepPage: false);

  void share(BuildContext context) {
    // final RenderObject? box = context.findRenderObject();
    Share.share(
      text,
      subject: subject,
      // sharePositionOrigin: box?.localToGlobal(Offset.zero) & box?.size,
    );
  }

  void updateList(int value) async {
    MovementProvider moveProvider = MovementProvider();
    CardsProvider cardsProvider = CardsProvider();
    List<Movement> getMovesAsync = await moveProvider.getMoves(card: value);

    sum = 0;
    getMovesAsync.forEach((element) {
      sum += element.amount;
    });

    final card = cardsProvider.getUserCards()[value];
    setState(() {
      getMoves = getMovesAsync;
      cardIndex = value;
      dateMY = DateTime.now();
      text =
          'Mi número de cuenta en ${bankName(card)} \nes ${card.nro}\n y CCI es ${card.nroCCI}';
      subject = card.title;
    });
    await changeCard(value);
  }

  @override
  void initState() {
    super.initState();
    updateList(0);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    // Brightness theme = MediaQuery.of(context).platformBrightness;
    Brightness theme =
        MediaQueryData.fromWindow(WidgetsBinding.instance!.window)
            .platformBrightness;
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => HomePageProvider())
      ],
      child: Scaffold(
        body: Stack(
          children: [
            PageView(
              physics: BouncingScrollPhysics(),
              controller: _pageController,
              onPageChanged: (page) {
                setState(() {
                  pageIndex = page;
                  // navBarState = _bottomNavigationKey.currentState;
                  // navBarState.setPage(page);
                });
              },
              children: [
                WillPopScope(
                  onWillPop: () async => false,
                  child: HomeScreen(
                    cardIndex: cardIndex,
                    getMoves: getMoves,
                    share: share,
                    updateList: updateList,
                    sum: sum,
                    dateMY: dateMY,
                  ),
                ),
                Container(
                  child: WalletScreen(),
                ),
                Container(
                  child: TransferScreen(),
                ),
                Container(
                  child: MoreScreen(),
                ),
              ],
            ),
            Positioned(
              bottom: 0.0,
              left: 5.0,
              right: 5.0,
              child: Center(
                child: Container(
                  margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
                  height: responsive.hp(7.0),
                  width: responsive.wp(80.0),
                  constraints: BoxConstraints(
                    maxHeight: 100.0,
                    maxWidth: 500.0,
                  ),
                  decoration: BoxDecoration(
                      color: theme == Brightness.dark
                          ? Colors.black
                          : Colors.grey[200],
                      borderRadius: BorderRadius.circular(50.0)),
                  child: Row(
                    children: [
                      Expanded(
                        child: Center(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _pageController.jumpToPage(0);
                              });
                            },
                            child: FaIcon(
                              FontAwesomeIcons.wallet,
                              color: pageIndex == 0
                                  ? theme == Brightness.dark
                                      ? Colors.blue[400]
                                      : Colors.blue[800]
                                  : null,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Center(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _pageController.jumpToPage(1);
                              });
                            },
                            child: FaIcon(
                              FontAwesomeIcons.creditCard,
                              color: pageIndex == 1
                                  ? theme == Brightness.dark
                                      ? Colors.blue[400]
                                      : Colors.blue[800]
                                  : null,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Center(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _pageController.jumpToPage(2);
                              });
                            },
                            child: FaIcon(
                              FontAwesomeIcons.exchangeAlt,
                              color: pageIndex == 2
                                  ? theme == Brightness.dark
                                      ? Colors.blue[400]
                                      : Colors.blue[800]
                                  : null,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Center(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _pageController.jumpToPage(3);
                              });
                            },
                            child: FaIcon(
                              FontAwesomeIcons.bars,
                              color: pageIndex == 3
                                  ? theme == Brightness.dark
                                      ? Colors.blue[400]
                                      : Colors.blue[800]
                                  : null,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _middle(double width, double height) {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: width * 0.1,
          vertical: height * 0.03,
        ),
        child: MiddleHomeWidget(
          dateMY: dateMY,
          sum: sum,
          cardIndex: cardIndex,
        ),
      ),
    );
  }
}

class HomeScreen extends StatelessWidget {
  final Function share;
  final List<Movement> getMoves;
  final int cardIndex;
  final Function updateList;
  final double sum;
  final DateTime dateMY;
  const HomeScreen({
    required this.share,
    required this.cardIndex,
    required this.getMoves,
    required this.updateList,
    required this.sum,
    required this.dateMY,
  });
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            Expanded(
              child: HeaderCurvo(),
              flex: 3,
            ),
            Expanded(
              child: Container(),
              flex: 4,
            )
          ],
        ),
        Column(
          children: [
            Expanded(
              child: HeaderCustom(
                  avatar:
                      'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                  username: 'José'),
              flex: 4,
            ),
            Expanded(
              child: GestureDetector(
                onLongPress: () async {
                  // _settingModalBottomSheet(context);
                  share(context);
                },
                child: Center(
                  child: CardList(
                    callback: updateList,
                  ),
                ),
              ),
              flex: 5,
            ),
            Expanded(
              child: Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.1,
                    vertical: MediaQuery.of(context).size.height * 0.03,
                  ),
                  child: MiddleHomeWidget(
                    dateMY: dateMY,
                    sum: sum,
                    cardIndex: cardIndex,
                  ),
                ),
              ),
              flex: 2,
            ),
            Expanded(
              child: Container(
                padding:
                    EdgeInsets.all(MediaQuery.of(context).size.width * 0.01),
                child: ListItems(
                  MediaQuery.of(context).orientation,
                  getMoves,
                  cardIndex,
                  updateList,
                  isIn: false,
                ),
              ),
              flex: 6,
            ),
            Expanded(
              child: Container(),
              flex: 2,
            ),
          ],
        ),
      ],
      alignment: AlignmentDirectional.center,
    );
  }
}

class WalletScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('Tarjetas');
    return Center(child: Text('Tarjetas'));
  }
}

class TransferScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('Transferencia');
    return Center(child: Text('Transferencia'));
  }
}

class MoreScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('Más');
    return Center(child: Text('Más'));
  }
}
