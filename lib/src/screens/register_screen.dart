import 'package:cached_network_image/cached_network_image.dart';
import 'package:finance_app/src/constants/api.dart';
import 'package:finance_app/src/providers/google_signin_provider.dart';
import 'package:finance_app/src/providers/login_provider.dart';
import 'package:finance_app/src/screens/pin_screen.dart';
import 'package:finance_app/src/utils/responsive.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final formKey = GlobalKey<FormState>();
  final pinController = TextEditingController();
  final confirmPinController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser;
    // final responsive = Responsive.of(context);
    // ignore: todo
    //TODO check register using Future Builder
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Center(
          child: SafeArea(
              child: OrientationBuilder(
            builder: (_, orientation) => orientation == Orientation.portrait
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      LeftSideRegister(
                        user: user,
                        formKey: formKey,
                        confirmPinController: confirmPinController,
                        pinController: pinController,
                      ),
                      RightSide(
                        formKey: formKey,
                        confirmPinController: confirmPinController,
                        pinController: pinController,
                      ),
                    ],
                  )
                : Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      LeftSideRegister(
                        user: user,
                        formKey: formKey,
                        confirmPinController: confirmPinController,
                        pinController: pinController,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: RightSide(
                          formKey: formKey,
                          confirmPinController: confirmPinController,
                          pinController: pinController,
                        ),
                      ),
                    ],
                  ),
          )),
        ),
      ),
    );
  }
}

class LeftSideRegister extends StatelessWidget {
  const LeftSideRegister({
    Key? key,
    this.user,
    required this.formKey,
    required this.confirmPinController,
    required this.pinController,
  }) : super(key: key);
  final User? user;
  final GlobalKey<FormState> formKey;
  final TextEditingController pinController;
  final TextEditingController confirmPinController;

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Expanded(
      child: SizedBox(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              responsive.orientation != Orientation.landscape
                  ? SizedBox(
                      height: responsive.hp(20.0),
                    )
                  : const SizedBox(),
              Hero(
                tag: 'userImg',
                child: CircleAvatar(
                  maxRadius: responsive.dp(5.0),
                  backgroundImage: CachedNetworkImageProvider(user?.photoURL ??
                      'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'),
                ),
              ),
              SizedBox(
                height: responsive.hp(10.0),
              ),
              Text(
                user?.displayName ?? 'Usuario',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: responsive.dp(1.8),
                ),
              ),
              SizedBox(
                height: responsive.hp(5.0),
              ),
              Container(
                  constraints: const BoxConstraints(
                    maxWidth: 500.0,
                  ),
                  // height: responsive.hp(20.0),

                  width: responsive.wp(50.0),
                  child: RegisterForm(
                      formKey: formKey,
                      pinController: pinController,
                      responsive: responsive,
                      confirmPinController: confirmPinController)
                  // child: Consumer<LoginProvider>(
                  //   builder: (context, data, _) => data.isRegister
                  //       ? RegisterForm(
                  //           formKey: formKey,
                  //           pinController: pinController,
                  //           responsive: responsive,
                  //           confirmPinController: confirmPinController)
                  //       : PinForm(
                  //           formKey: formKey,
                  //           pinController: pinController,
                  //           responsive: responsive),
                  // ),
                  // child: FutureBuilder(
                  //   builder: (BuildContext context,
                  //       AsyncSnapshot<Map<String, dynamic>> snapshot) {
                  //     if (snapshot.hasData) {
                  //       return Placeholder();
                  //     }
                  //     if (snapshot.hasError) {
                  //       return RegisterForm(
                  //           formKey: formKey,
                  //           pinController: pinController,
                  //           responsive: responsive,
                  //           confirmPinController: confirmPinController);
                  //     }
                  //     //   if (!snapshot.data!.isRegister) {
                  //     //     return RegisterForm(
                  //     //         formKey: formKey,
                  //     //         pinController: pinController,
                  //     //         responsive: responsive,
                  //     //         confirmPinController: confirmPinController);
                  //     //   }
                  //     //   return const Placeholder();
                  //     // }
                  //     // return RegisterForm(
                  //     //         formKey: formKey,
                  //     //         pinController: pinController,
                  //     //         responsive: responsive,
                  //     //         confirmPinController: confirmPinController);
                  //     return const Center(child: CircularProgressIndicator());
                  //   },
                  // ),
                  ),
            ],
          ),
        ),
      ),
    );
  }
}

class PinForm extends StatelessWidget {
  const PinForm({
    Key? key,
    required this.formKey,
    required this.pinController,
    required this.responsive,
  }) : super(key: key);

  final GlobalKey<FormState> formKey;
  final TextEditingController pinController;
  final Responsive responsive;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          TextFormField(
            controller: pinController,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Debe ingresar su PIN';
              }
              if (value.length != 6) {
                return 'El PIN debe tener sólo 6 dígitos';
              }
            },
            obscureText: true,
            decoration: InputDecoration(
              errorMaxLines: 2,
              labelText: 'Escribe tu PIN de seguridad',
              labelStyle: TextStyle(
                fontSize: responsive.dp(1.3),
              ),
            ),
            style: TextStyle(
              fontSize: responsive.dp(1.5),
            ),
          ),
          const SizedBox(height: 30.0),
        ],
      ),
    );
  }
}

class RegisterForm extends StatelessWidget {
  const RegisterForm({
    Key? key,
    required this.formKey,
    required this.pinController,
    required this.responsive,
    required this.confirmPinController,
  }) : super(key: key);

  final GlobalKey<FormState> formKey;
  final TextEditingController pinController;
  final Responsive responsive;
  final TextEditingController confirmPinController;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          TextFormField(
            controller: pinController,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Debe ingresar su PIN';
              }
              if (value.length != 6) {
                return 'El PIN debe tener sólo 6 dígitos';
              }
            },
            obscureText: true,
            decoration: InputDecoration(
              errorMaxLines: 2,
              labelText: 'Escribe tu PIN de seguridad',
              labelStyle: TextStyle(
                fontSize: responsive.dp(1.3),
              ),
            ),
            style: TextStyle(
              fontSize: responsive.dp(1.5),
            ),
          ),
          TextFormField(
            controller: confirmPinController,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Debe ingresar su PIN';
              }
              if (value != pinController.text) {
                return 'El PIN debe coincidir';
              }
            },
            obscureText: true,
            decoration: InputDecoration(
              errorMaxLines: 2,
              labelText: 'Repite tu PIN',
              labelStyle: TextStyle(
                fontSize: responsive.dp(1.3),
              ),
            ),
            style: TextStyle(
              fontSize: responsive.dp(1.5),
            ),
          ),
          const SizedBox(height: 30.0),
        ],
      ),
    );
  }
}

class RightSide extends StatelessWidget {
  const RightSide(
      {Key? key,
      required this.formKey,
      required this.confirmPinController,
      required this.pinController})
      : super(key: key);
  final GlobalKey<FormState> formKey;
  final TextEditingController pinController;
  final TextEditingController confirmPinController;

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<GoogleSignInProvider>(context, listen: true);
    final responsive = Responsive.of(context);
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: responsive.dp(10.0),
            child: ElevatedButton(
              onPressed: () async {
                if (formKey.currentState!.validate()) {
                  formKey.currentState!.save();
                  await Navigator.pushNamed(context, '/home');
                }
              },
              child: Padding(
                padding: EdgeInsets.all(
                  responsive.dp(responsive.isMobile ? 0.0 : 1.1),
                ),
                child: Text(
                  'Empezar',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: responsive.dp(1.5),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: responsive.hp(1.0)),
          SizedBox(
            width: responsive.dp(20.0),
            child: TextButton(
              onPressed: () async {
                await provider.googleLogout();
                await Navigator.pushNamed(context, '/login');
              },
              child: Text(
                'Cambiar a otra cuenta',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: responsive.dp(1.2),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
