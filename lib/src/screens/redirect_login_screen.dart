import 'package:finance_app/src/constants/api.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class RedirectScreen extends StatelessWidget {
  const RedirectScreen({
    Key? key,
  }) : super(key: key);

  // final api = API();
  // final String uid;

  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser;
    return FutureBuilder(
      future:
          API().postFetch('/v1/user/isRegistered', {'uid': user?.uid ?? ''}),
      builder: (context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
        if (snapshot.hasData) {
          print(snapshot.data);
          Navigator.pushNamed(context, '/pin');
        }
        if (snapshot.hasError) {
          Navigator.pushNamed(context, '/register');
        }
        return Container();
      },
    );
  }
}
