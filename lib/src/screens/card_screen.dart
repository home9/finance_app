import 'package:finance_app/src/models/card_model.dart';
import 'package:finance_app/src/widgets/card.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CardMoveScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> args =
        ModalRoute?.of(context)?.settings.arguments as Map<String, dynamic>;
    final height = args['height'] as double;
    final width = args['width'] as double;
    final CardBankModel cardData = args['cardData'] as CardBankModel;
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(40),
                      bottomRight: Radius.circular(40),
                    ),
                    color: Colors.black,
                  ),
                  child: Center(
                    child: Hero(
                      child: CardBank(
                        height,
                        width,
                        cardData,
                      ),
                      tag: 'Card',
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(),
              ),
            ],
          ),
          Positioned(
            top: 20,
            left: 20,
            child: GestureDetector(
              onTap: () => Navigator.pop(context),
              child: const SafeArea(
                child: FaIcon(
                  FontAwesomeIcons.chevronLeft,
                  size: 20,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
