import 'package:finance_app/helpers/images.gen.dart';
import 'package:finance_app/src/models/user_firebase.dart';
import 'package:finance_app/src/providers/google_signin_provider.dart';
import 'package:finance_app/src/utils/device.dart';
import 'package:finance_app/src/utils/responsive.dart';
// import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:dio/dio.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

// void getHttp() async {
//   try {
//     var response =
//         await Dio().get('https://finance.backend.jsiapo.dev/api/location');
//     // print("response");
//     // print(response);
//   } catch (e) {
//     // print(e);
//   }
// }

Widget _buildImage(String assetName, double width, double height) {
  return WillPopScope(
    onWillPop: () async => false,
    child: Center(
      child: Container(
        padding: EdgeInsets.all(width),
        height: height,
        child: SvgPicture.asset(
          assetName,
        ),
      ),
    ),
  );
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  DevicePlatform dp = DevicePlatform();
  UserFirebase user = UserFirebase();
  TextEditingController txtcontroller = TextEditingController();

  @override
  void dispose() {
    txtcontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    Brightness theme =
        MediaQueryData.fromWindow(WidgetsBinding.instance!.window)
            .platformBrightness;
    return Scaffold(
      body: Container(
        color: const Color.fromARGB(200, 244, 184, 68),
        child: Center(
            child: Form(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                _buildImage(
                  IMG.icons.undrawMobilePayReSjb8SVG,
                  responsive.wp(responsive.orientation == Orientation.landscape
                      ? 2.0
                      : 5.0),
                  responsive.hp(responsive.orientation == Orientation.landscape
                      ? 60.0
                      : 70.0),
                ),
                SizedBox(height: responsive.hp(10.0)),
                // TextFormField(),
                // TextFormField(controller: txtcontroller),
                // TextButton(
                //   onPressed: () async {
                //     getHttp();
                //     dp.getDevicePlatform();
                //     txtcontroller.text = await dp.getDeviceDetail();
                //   },
                //   child: const Text('Botón'),
                // ),
                Consumer<GoogleSignInProvider>(
                  builder: (context, data, _) => ElevatedButton(
                    style: ButtonStyle(
                      padding: MaterialStateProperty.resolveWith(
                        (states) => const EdgeInsets.symmetric(
                          horizontal: 18.0,
                          vertical: 15.0,
                        ),
                      ),
                      backgroundColor: MaterialStateProperty.resolveWith(
                        (states) => theme == Brightness.dark
                            ? Colors.black
                            : Colors.white,
                      ),
                    ),
                    onPressed: () async {
                      // await data.googleSignIn();
                      if (data.isAuth) {
                        await Navigator.pushNamed(context, '/register');
                        // await Navigator.pushNamed(context, '/pin');
                      }
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        FaIcon(
                          FontAwesomeIcons.google,
                          color: theme == Brightness.dark
                              ? Colors.white
                              : Colors.black,
                          size: 18.0,
                        ),
                        const SizedBox(width: 15.0),
                        Text(
                          'Continuar con Google',
                          style: TextStyle(
                            fontSize: 18.0,
                            color: theme == Brightness.dark
                                ? Colors.white
                                : Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        )),
      ),
    );
  }
}
