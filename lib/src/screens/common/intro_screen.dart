import 'package:finance_app/helpers/images.gen.dart';
import 'package:finance_app/src/route.dart';
import 'package:finance_app/src/utils/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:introduction_screen/introduction_screen.dart';
// import 'package:firebase_auth/firebase_auth.dart';

class IntroScreen extends StatelessWidget {
  const IntroScreen({Key? key}) : super(key: key);

  Widget _buildImage(String assetName, double width) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(width),
        child: SvgPicture.asset(
          assetName,
        ),
      ),
      // alignment: Alignment.bottomCenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    // final user = FirebaseAuth.instance.currentUser;
    final responsive = Responsive.of(context);
    Brightness theme =
        MediaQueryData.fromWindow(WidgetsBinding.instance!.window)
            .platformBrightness;
    final pageDecoration = PageDecoration(
      titleTextStyle: TextStyle(
        fontSize: responsive.dp(2.5),
        fontWeight: FontWeight.w700,
      ),
      bodyTextStyle: TextStyle(
        fontSize: responsive.dp(1.8),
      ),
      descriptionPadding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.transparent,
      imagePadding: EdgeInsets.zero,
    );
    return WillPopScope(
      onWillPop: () async => false,
      child: IntroductionScreen(
        pages: [
          PageViewModel(
            title: 'Administra tus gastos',
            body: 'Lleva el control de tus compras y pagos',
            image: _buildImage(
              IMG.icons.walletUndrawSVG,
              responsive.dp(
                  responsive.orientation == Orientation.landscape ? 2.0 : 5.0),
            ),
            decoration: pageDecoration,
          ),
          PageViewModel(
            title: 'Incrementa tus ahorros!',
            body: 'Tener un control sobre tus gastos es AHORRAR',
            image: _buildImage(
              IMG.icons.undrawPayOnlineB1hkSVG,
              responsive.dp(
                  responsive.orientation == Orientation.landscape ? 2.0 : 8.0),
            ),
            decoration: pageDecoration,
          ),
          PageViewModel(
            title: 'Comparte tu número de cuenta facilmente',
            body: 'Para que puedan depositarte o'
                ' transferirte a tu cuenta bancaria',
            image: _buildImage(
              IMG.icons.undrawOnlineTransactions_02kaSVG,
              responsive.dp(
                  responsive.orientation == Orientation.landscape ? 2.0 : 5.0),
            ),
            decoration: pageDecoration,
          ),
          PageViewModel(
            title: 'Oculta tu número de cuenta a desconocidos',
            body: 'Sólo comparte tu cuenta con las personas que conozcas',
            image: _buildImage(
              IMG.icons.undrawSecuritySVG,
              responsive.dp(
                  responsive.orientation == Orientation.landscape ? 2.0 : 8.0),
            ),
            decoration: pageDecoration,
          ),
          PageViewModel(
            title: 'Ve al éxito',
            body: 'Y controla tu dinero',
            image: _buildImage(
              IMG.icons.undrawPersonalFinanceSVG,
              responsive.dp(
                  responsive.orientation == Orientation.landscape ? 2.0 : 8.0),
            ),
            decoration: pageDecoration,
          ),
        ],
        onDone: () => goTo(context, '/login'),
        onSkip: () => goTo(context, '/login'),
        showSkipButton: true,
        skip: const Text('Omitir'),
        next: const FaIcon(FontAwesomeIcons.arrowAltCircleRight),
        done:
            const Text('Listo', style: TextStyle(fontWeight: FontWeight.w600)),
        dotsDecorator: DotsDecorator(
            size: const Size.square(6.0),
            activeSize: const Size(30.0, 10.0),
            // ignore: todo
            //TODO update color whit Brightness
            activeColor: theme == Brightness.light ? Colors.teal : Colors.amber,
            color: Colors.grey,
            spacing: const EdgeInsets.symmetric(horizontal: 3.0),
            activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0))),
      ),
    );
  }
}
