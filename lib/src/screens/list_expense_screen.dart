import 'package:finance_app/src/widgets/chart_sample.dart';
import 'package:flutter/material.dart';
import 'package:finance_app/src/models/move.dart';
import 'package:finance_app/src/preferences/card.dart';
import 'package:finance_app/src/providers/move_provider.dart';
import 'package:finance_app/src/widgets/list_items_desc.dart';

class ExpensesScreen extends StatefulWidget {
  @override
  _ExpensesScreenState createState() => _ExpensesScreenState();
}

class _ExpensesScreenState extends State<ExpensesScreen> {
  List<Movement> getMoves = [];
  late int cardIndex;
  double _sum = 00.00;
  late Function updateList;

  void updateList2(int value) async {
    MovementProvider moveProvider = MovementProvider();
    List<Movement> getMovesAsync = await moveProvider.getMoves(card: value);
    double sum = 0.00;
    getMovesAsync.forEach((element) {
      sum += element.amount;
    });
    setState(() {
      getMoves = getMovesAsync;
      cardIndex = value;
      _sum = sum;
    });
  }

  Future<void> inital() async => updateList2(await getCardSharedPref());

  @override
  void initState() {
    inital();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;
    Map<String, dynamic> args =
        ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    setState(() {
      cardIndex = args['cardIndex'] as int;
    });
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'TRANSACCIONES',
          textScaleFactor: 1.0,
        ),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Center(
                      child: ListItems(
                    orientation,
                    getMoves,
                    cardIndex,
                    updateList2,
                    isIn: false,
                  )),
                  flex: 8,
                ),
              ],
            ),
            flex: 4,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 20.0),
                    width: double.infinity,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: LineChartSample2(),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            flex: 4,
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Total :',
                    textScaleFactor: 1.0,
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    '${_sum.toStringAsFixed(2)}',
                    textScaleFactor: 1.0,
                    style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            ),
            flex: 1,
          ),
        ],
      ),
    );
  }
}
