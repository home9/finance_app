import 'package:finance_app/src/constants/bank.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

extension FinancialServiceExtension on FinancialService {
  IconData get icon {
    switch (this) {
      case FinancialService.visa:
        return FontAwesomeIcons.ccVisa;
      case FinancialService.americanExpress:
        return FontAwesomeIcons.ccAmex;
      case FinancialService.masterCard:
        return FontAwesomeIcons.ccMastercard;
      default:
        return FontAwesomeIcons.ccVisa;
    }
  }
}
